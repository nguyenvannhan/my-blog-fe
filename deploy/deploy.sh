#!/usr/bin/env bash

chown -R nginx:nginx /home/gialangcoding.com/public_html

find . -type f -exec chmod 644 {} \;
find . -type d -exec chmod 755 {} \;

cp deploy/pm2.config.js ecosystem.config.js

pm2 restart GialangCodingBlog
