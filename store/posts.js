export const state = () => ({
  postList: {
    data: [],
    meta: {},
  },
  postItem: null,
})

export const mutations = {
  SET_POST_LIST(state, { postList, loadMore }) {
    if (loadMore) {
      const newPostList = state.postList.data.concat(postList.data)

      state.postList = {
        ...postList,
        data: newPostList,
      }
    } else {
      state.postList = postList
    }
  },

  SET_POST_ITEM(state, postItem) {
    state.postItem = postItem
  },
}

export const actions = {
  async getPostList({ commit }, payload = {}) {
    const response = await this.$axios.get('/posts', {
      params: payload.params,
    })

    commit('SET_POST_LIST', {
      postList: response.data,
      loadMore: payload.loadMore,
    })
  },

  async getPostItem({ commit }, payload = {}) {
    const response = await this.$axios.get(`/posts/${payload.slug}`)
    commit('SET_POST_ITEM', response.data.data)
  },

  async getPostListByCateSlug({ commit, dispatch, rootGetters }, payload = {}) {
    await dispatch(
      'categories/getCategoryItem',
      {
        slug: payload.cate_slug,
      },
      { root: true }
    )

    const cateItem = rootGetters['categories/getCateItem']

    const response = await this.$axios.get('/posts', {
      params: {
        category_ids: cateItem.id,
        ...payload.params,
      },
    })

    commit('SET_POST_LIST', {
      postList: response.data,
      loadMore: payload.loadMore,
    })
  },
}
