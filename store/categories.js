export const state = () => ({
  cateItem: {},
  cateList: [],
})

export const mutations = {
  SET_CATEGORY_LIST(state, categoryList) {
    state.cateList = categoryList
  },

  SET_CATEGORY_ITEM(state, categoryItem) {
    state.cateItem = categoryItem
  },
}

export const actions = {
  async getCategoryList({ commit }, payload = {}) {
    const response = await this.$axios
      .get('/categories', {
        params: payload.params,
      })
      .then((res) => {
        return res
      })
      .catch((err) => {
        return err
      })

    commit('SET_CATEGORY_LIST', response.data.data)
  },

  async getCategoryItem({ commit }, payload = {}) {
    const response = await this.$axios.get(`/categories/${payload.slug}`)

    commit('SET_CATEGORY_ITEM', response.data.data)
  },
}

export const getters = {
  getCateItem(state) {
    return state.cateItem
  },
}
