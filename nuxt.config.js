export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'front-end',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['@/assets/sass/app.scss'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: ['@/plugins/globalComponents'],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    [
      '@nuxtjs/fontawesome',
      {
        component: 'fa',
        suffix: true,
      },
    ],
    // '@nuxtjs/google-analytics',
  ],

  fontawesome: {
    icons: {
      solid: true,
      brands: true,
    },
  },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {
    baseURL: process.env.API_BASE_URL,
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
    },
    proxyHeaders: false,
    credentials: false,
  },

  // Content module configuration (https://go.nuxtjs.dev/config-content)
  content: {},

  // googleAnalytics: {
  //   id: process.env.GOOGLE_ANALYTICS_ID,
  //   dev: true,
  //   checkDuplicatedScript: true,
  //   debug: true,
  // },

  publicRuntimeConfig: {
    //   axios: {
    //     baseURL: process.env.API_BASE_URL,
    //   },
    // googleAnalytics: {
    //   id: process.env.GOOGLE_ANALYTICS_ID,
    //   dev: true,
    //   checkDuplicatedScript: true,
    //   debug: true,
    // },
  },

  privateRuntimeConfig: {
    //   axios: {
    //     baseURL: process.env.API_BASE_URL,
    //   },
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
}
